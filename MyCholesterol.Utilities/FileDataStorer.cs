﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyCholesterol.Core;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace MyCholesterol.Utilities
{
    public class FileDataStorer : IDataStorer
    {
        private string file;
        private const string DATA_FILE_NAME = "MyCholesterol.data"; 

        public FileDataStorer(string file = null)
        {
            this.file = file ?? GetDefaultFile();
        }

        public IEnumerable<Values> Read()
        {
            using (var stream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                return (IEnumerable<Values>)new BinaryFormatter().Deserialize(stream);
        }

        public void Save(IEnumerable<Values> values)
        {
            using (var stream = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None))
                new BinaryFormatter().Serialize(stream, values);
        }

        private string GetDefaultFile()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DATA_FILE_NAME);
        }
    }
}
