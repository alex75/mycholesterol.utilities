﻿using MyCholesterol.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCholesterol.Utilities
{
    public interface IDataStorer
    {
        void Save(IEnumerable<Values> values);
        IEnumerable<Values> Read();
    }
}
