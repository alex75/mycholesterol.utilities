﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using Should;
using MyCholesterol.Utilities;
using MyCholesterol.Core;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace MyCholesterol.UnitTests
{
    [TestFixture]
    public class FileDataStorerTest
    {
        [Test]
        public void Constructor_when_FileIsNull()
        {
            FileDataStorer dataStorer = new FileDataStorer(null);

            Assert.IsNotNull(dataStorer);
        }

        [Test]
        public void Constructor_when_FileIsNotNull()
        {
            string file = "file.ext";
            FileDataStorer dataStorer = new FileDataStorer(file);

            Assert.IsNotNull(dataStorer);
        }

        [Test]
        public void Read_when_DataIsStoredInBinaryFormat()
        {
            string file = "test.data";            
            IList<Values> data = CreateExampleData();

            using (var stream = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None))
                new BinaryFormatter().Serialize(stream, data);

            var dataStorer = new FileDataStorer(file);

            IEnumerable<Values> valuesEnumerator = dataStorer.Read();

            Assert.IsNotNull(valuesEnumerator);

            var values = valuesEnumerator.ToList();
            values.Count.ShouldEqual(data.Count);
        }

        [Test]
        public void Save_should_CreateTheDestinationFile()
        {
            string file = "test.data";            
            File.Delete(file);
            var data = CreateExampleData();

            try
            {
                FileDataStorer dataStorer = new FileDataStorer(file);
                dataStorer.Save(data);
                string a = Directory.GetCurrentDirectory();
                Assert.IsTrue(File.Exists(file));
            }
            finally
            {
                File.Delete(file);
            }            
        } 

        #region helper  

        private IList<Values> CreateExampleData()
        {
            return
            new List<Values>() {
                new Values(1),
                new Values(1, 2),
                new Values(1, 2, 3),
                new Values(100, 101, 102, 103)
            };
        }

        #endregion
    }
}
